% \iffalse meta-comment
%   arara: lualatex: { files: [tikzman.ins] }
%   arara: lualatex until !found('log', 'undefined references')
%   arara: makeindex
%   arara: lualatex
%
%% tikzman.dtx
%% Copyright 2024 Max Günther
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   https://www.latex-project.org/lppl.txt
% and version 1.3c or later is part of all distributions of LaTeX
% version 2008 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Max Günther
%
% This work consists of the files tikzman.dtx and tikzman.ins and the
% derived file tikzman.sty.
%
% \fi
% \iffalse
%<package>\NeedsTeXFormat{LaTeX2e}
%<package>\ProvidesPackage{tikzman}
%<package> [2024/02/12 v1.0.1 Draw a stick man and a duck in TikZ]
%<*driver>
\documentclass[11pt]{ltxdoc}
\usepackage{geometry}
\usepackage[linkcolor=black]{hyperref}
\usepackage[T1]{fontenc}
\usepackage{fontspec}
\newfontface{\Vasallo}{vasallo-regular.ttf}
\usepackage{titlesec}
\titleformat*{\section}{\Large\Vasallo}
\titleformat*{\subsection}{\large\Vasallo}
\usepackage{microtype}
\usepackage{lmodern}
\usepackage{tikzman}
\EnableCrossrefs
\CodelineIndex
\RecordChanges
\begin{document}
\DocInput{tikzman.dtx}
\end{document}
%</driver>
% \fi
%
% \changes{v1.0.1}{2024-02-12}{Fix issue with rump of duck}
% \changes{v1.0.0}{2024-02-12}{Release TikZman v1.0.0}
% \changes{v0.1.0}{2024-02-07}{Add tikzman.dtx}
%
% \GetFileInfo{tikzman.sty}
%
% \newcommand\TikZ{Ti\emph{k}Z}
% \newcommand\pkgname{\TikZ man}
% \newcommand\TikZducks{\TikZ ducks}
%
% \begin{titlepage}
%   \vspace*{3cm}
%   \begin{center}
%     {\Huge\Vasallo
%       \pkgname\\\medskip\huge A man and his duck
%     }\\\vspace{.75cm}\Large\mdseries
%     Max Günther\\
%     \texttt{code-mg@mailbox.org}\\\vspace{.75cm} \today
%   \end{center}
%   \vfill \centering
%   This documentation refers to \pkgname~\fileversion, last revised
%   on \filedate.
% \end{titlepage}
%
% \thispagestyle{empty}
%
% \vspace*{3cm}
% \hfill {\large\itshape%
%   \Vasallo For my father, who introduced me to the world of \TeX.
% }
% \clearpage
%
% \setcounter{page}{1}
%
% \section{Usage}
%
% The package \pkgname{} provides two commands for drawing the man and
% his duck, \cs{man} and \cs{ducky}\footnote{Please note that
% \pkgname{} will also define the command \cs{duck} as an alias for
% \cs{ducky}. However, this behaviour is turned off as soon as the
% package \TikZducks{} is loaded.}, respectively.
%
% Both have to be used inside a \texttt{tikzpicture} environment, like
% in the following example:
%
% \bigskip
% \noindent
% \begin{minipage}{.5\textwidth}
%   \begin{verbatim}
%     \begin{tikzpicture}
%       \man;
%     \end{tikzpicture}
%   \end{verbatim}
% \end{minipage}%
% \begin{minipage}{.5\textwidth}
%   \centering
%   \begin{tikzpicture}[scale=.9125]
%     \man;
%   \end{tikzpicture}
% \end{minipage}
%
% \bigskip
% \noindent
% \begin{minipage}{.5\textwidth}
%   \begin{verbatim}
%     \begin{tikzpicture}
%       \ducky;
%     \end{tikzpicture}
%   \end{verbatim}
% \end{minipage}%
% \begin{minipage}{.5\textwidth}
%   \centering
%   \begin{tikzpicture}[scale=.9125]
%     \ducky;
%   \end{tikzpicture}
% \end{minipage}
%
% \StopEventually{}
%
% \section{Implementation}
%
% \subsection{Package loading}
%
%    \begin{macrocode}
\RequirePackage{tikz}
%    \end{macrocode}
%
% \subsection{Main code}
% \begin{macro}{\man} This macro is used for drawing the man.  We
%   start by defining some internal variables for controlling the
%   style of the man.
%
%    \begin{macrocode}
\def\tikzman@legoffset{.1375cm}
\def\tikzman@leglength{.9cm}
\def\tikzman@feetlength{.09cm}
%    \end{macrocode}
%
%   It is now time to actually define the macro \cs{man}:
%
%    \begin{macrocode}
\newcommand\man{%
  \begin{scope}[ultra thick,fill=white,cap=round]
    \foreach \x/\i in {\tikzman@legoffset/1, -\tikzman@legoffset/-1} {%
      \draw (\x,-.5cm) --
      (\x,-.5cm-\tikzman@leglength) --
      (\x+\i*\tikzman@feetlength,-.5cm-\tikzman@leglength);
    };

    \foreach \i in {1, -1} {%
      \draw (\i*.2cm,.45cm) .. controls (\i*.35cm,.35cm) and
      (\i*.45cm,0).. (\i*.4625cm,-.425cm);
    };

    \filldraw (0,0) ellipse [x radius=.35cm,y radius=.55cm];
    \filldraw (0,.7375cm) circle [radius=.21cm];
  \end{scope}
}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\ducky} This macro is used for drawing the duck.  Just
%   like in the case of \cs{man}, we start by defining some internal
%   variables.
%
%    \begin{macrocode}
\def\tikzducky@legpositionx{-.08cm}
\def\tikzducky@leglength{.5cm}
\def\tikzducky@legoffset{.15cm}
\def\tikzducky@feetlength{.0375cm}
%    \end{macrocode}
%
%   We can now define \cs{ducky}.
%
%    \begin{macrocode}
\newcommand\ducky{%
  \begin{scope}[ultra thick,cap=round]
    \filldraw [rounded corners=.25pt] (-.3cm,.525cm) -- (-.375cm,.5825cm)
    -- (-.3cm,.625cm);

    \foreach \o in {0, \tikzducky@legoffset} {%
      \draw (\tikzducky@legpositionx+\o,.2cm) --
      (\tikzducky@legpositionx+\o,.2cm-\tikzducky@leglength) --
      (\tikzducky@legpositionx+\o-\tikzducky@feetlength,.2cm
      -\tikzducky@leglength);
    };

    \filldraw [fill=white] (.3cm,.5cm) .. controls (.25cm,.05cm) and
    (-.275cm,.05cm) .. (-.3cm,.5cm) -- (-.3cm,.6cm) .. controls
    (-.3cm,.9cm) and (-.05cm,.9cm) .. (-.05cm,.65cm) .. controls
    (-.05cm,.45cm) and (.1cm,.5cm) .. (.3cm,.575cm) -- cycle;

    \fill (-.2cm,.675cm) circle [radius=.9pt];
  \end{scope}
}
%    \end{macrocode}
% \end{macro}
%
% When the package \TikZducks{} is \emph{not} loaded, we also define the
% alias \cs{duck}.
%
%    \begin{macrocode}
\AtBeginDocument{\@ifpackageloaded{tikzducks}{%
  }{%
    \newcommand\duck{\ducky}
  }
}
%    \end{macrocode}
%
% \Finale
%
% \endinput
% Local Variables:
% mode: doctex
% TeX-engine: luatex
% TeX-master: t
% TeX-master: t
% End:
